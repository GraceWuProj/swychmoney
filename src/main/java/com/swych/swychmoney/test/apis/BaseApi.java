package com.swych.swychmoney.test.apis;

import com.swych.swychmoney.test.cases.Utils;
import io.restassured.path.json.JsonPath;
import io.restassured.path.json.config.JsonPathConfig;
import io.restassured.response.Response;
import org.json.simple.JSONObject;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;

public class BaseApi {

    private int statusCode;
    private int errorCode;
    private String errorMessage;
    private String status;
    private String numberRec;
    private String body;
    private String userId;
    private String accessToken;
    private String first_name;
    private String last_name;
    private String update;
    private String read;
    private String add;
    private String password;
    private String is_admin;
    private String partner_id;
    private BigDecimal nodeBalance;
    private BigDecimal accountBalance;
    private String extTxProduct;
    private String extTxId;

    public void setResp(Response resp) {

        statusCode = resp.getStatusCode();
        errorMessage = resp.jsonPath().get("errorMessage");
        status =  resp.jsonPath().get("status");
        userId = resp.jsonPath().get("userId");
        accessToken = resp.jsonPath().get("accessToken");
        first_name = resp.jsonPath().get("first_name");
        last_name = resp.jsonPath().get("last_name");
        read = resp.jsonPath().get("readAccess");
        add = resp.jsonPath().get("addAccess");
        update = resp.jsonPath().get("updateAccess");
        is_admin = resp.jsonPath().get("adminAccess");
        password = resp.jsonPath().get("password");
        extTxProduct = resp.jsonPath().get("extTxProduct");
        extTxId = resp.jsonPath().get("extTxId");

JsonPath jp = resp.jsonPath();  // for debug

        HashMap hmap = resp.jsonPath()
                .using(new JsonPathConfig(JsonPathConfig.NumberReturnType.BIG_DECIMAL))
                .get();
        nodeBalance = (BigDecimal)hmap.get("Node Balance");
        accountBalance = (BigDecimal) hmap.get("Account Balance");

        if(2 == statusCode/100) {   // statusCode might be 2XX for success
            List<JSONObject> rec = resp.jsonPath().getList("logs");
            if (null != rec)
                numberRec = String.valueOf(rec.size());
        }
        body = Utils.getJsonPrettyPrint(resp.getBody().asString());
        System.out.println(this.body);

    }

    public int getStatusCode() {
        return statusCode;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getStatus() {
        return status;
    }

    public String getNumberRec(){ return numberRec;}

    public String getBody(){return body;}

    public String getUserId() { return userId;}

    public String getAccessToken(){return accessToken;}

    public String getPassword(){return password;}

    public String getFirstname(){return first_name;};

    public String getLastname(){return last_name;}

    public String getReadstatus(){return read;}

    public String getAddstatus(){return add;}

    public String getUpdatestatus(){return update;}

    public String getIsadminstatus(){return is_admin;}

    public String getPartner_id(){return partner_id;}

    public BigDecimal getNodeBalance(){return nodeBalance;}

    public BigDecimal getAccBalance(){return accountBalance;}

    public String getextTxProduct() {return extTxProduct;}

    public String getextTxId() {return extTxId;}





}
