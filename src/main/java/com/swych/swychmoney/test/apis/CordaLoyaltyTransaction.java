package com.swych.swychmoney.test.apis;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import static io.restassured.RestAssured.given;

public class CordaLoyaltyTransaction extends BaseApi {

    public Response createTrans(String correlationId, String extTxProduct, String extTxId, String extTxTimestamp){
        RestAssured.baseURI="http://34.70.208.49:8080";
        Response response = null;
        RequestSpecification request = given();

        JSONObject childJSON = new JSONObject();
        childJSON.put("FINABLR", "1.0");
        childJSON.put("AutoTestUser001", "0.01");
        childJSON.put("AutoTestUser002","0.01");

        JSONObject requestParams = new JSONObject();
        requestParams.put("correlationId", correlationId);
        requestParams.put("extTxProduct", extTxProduct);
        requestParams.put("extTxId", extTxId);
        requestParams.put("extTxTimestamp", extTxTimestamp);
        requestParams.put("toFinablrIdsAndQuantities",childJSON);

        response =given()
                .header("Content-Type", "application/json")
                .body(requestParams.toJSONString())
                .post("/api/loyaltytx");

        setResp(response);
        return response;

    }

    public Response viewTransaction(String extTxProduct, String extTxId){
        RestAssured.baseURI="http://34.70.208.49:8080";
        Response response = null;
        RequestSpecification request = given();

        response =given()
                .header("Content-Type", "application/json")
                .get("/api/loyaltytx?extTxProduct=" + extTxProduct + "&extTxId=" + extTxId);

        setResp(response);
        return response;

    }


}
