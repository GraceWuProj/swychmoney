package com.swych.swychmoney.test.apis;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;

import static io.restassured.RestAssured.given;

public class CordaMint extends BaseApi {
    public Response getCirculatingSupply(){
        RestAssured.baseURI="http://35.222.170.73:8080";
        Response response = null;
        RequestSpecification request = given();

        response =given()
                .header("Content-Type", "application/json")
                .get("/api/shokens/circulating-supply");

        setResp(response);
        return response;
    }

    public Response getMintBalance(){
        RestAssured.baseURI="http://35.222.170.73:8080";
        Response response = null;
        RequestSpecification request = given();

        response =given()
                .header("Content-Type", "application/json")
                .get("/api/shokens/node-balance");

        setResp(response);
        return response;
    }



}
