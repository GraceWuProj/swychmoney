package com.swych.swychmoney.test.apis;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import static io.restassured.RestAssured.given;

public class CordaWallet extends BaseApi {
    public Response createAccount(String finablrId){
        RestAssured.baseURI="http://104.154.246.25:8080";
        Response response = null;
        RequestSpecification request = given();

        response =given()
                .header("Content-Type", "application/json")
                .post("/api/accounts?finablrId=" + finablrId);

        setResp(response);
        return response;

    }

    public Response getAccount(String finablrId){
        RestAssured.baseURI="http://104.154.246.25:8080";
        Response response = null;
        RequestSpecification request = given();

        response =given()
                .header("Content-Type", "application/json")
                .get("/api/accounts?finablrId=" + finablrId);

        setResp(response);
        return response;

    }

    public Response getAccountBalance(String finablrId){
        RestAssured.baseURI="http://104.154.246.25:8080";
        Response response = null;
        RequestSpecification request = given();

        response =given()
                .header("Content-Type", "application/json")
                .get("/api/wallet/account-balance?finablrId=" + finablrId);

        setResp(response);
        return response;

    }

    public Response transferShoken(String correlationId){
        RestAssured.baseURI="http://104.154.246.25:8080";
        Response response = null;
        RequestSpecification request = given();

        JSONObject childJSON = new JSONObject();
        childJSON.put("AutoTestUser003", 0.001);
        childJSON.put("AutoTestUser004",0.001);

        JSONObject requestParams = new JSONObject();
        requestParams.put("fromFinablrId", "AutoTestUser001");
        requestParams.put("correlationId", correlationId);
        requestParams.put("toFinablrIdsAndQuantities",childJSON);


        response =given()
                .header("Content-Type", "application/json")
                .body(requestParams.toJSONString())
                .post("/api/wallet/transfer-shokens");

        setResp(response);
        return response;

    }

    public Response revokeShoken(String correlationId){
        RestAssured.baseURI="http://104.154.246.25:8080";
        Response response = null;
        RequestSpecification request = given();

        JSONObject childJSON = new JSONObject();
        childJSON.put("FinablrUser001", 0.01);
        childJSON.put("FinablrUser003",0.01);
        childJSON.put("FINABLR",1.0);

        JSONArray array = new JSONArray();
        array.add(childJSON);


        JSONObject requestParams = new JSONObject();
        requestParams.put("correlationId", correlationId);
        requestParams.put("fromFinablrIdsAndQuantities",array);


        response =given()
                .header("Content-Type", "application/json")
                .body(requestParams.toJSONString())
                .post("/api/wallet/revoke-shokens");

        setResp(response);
        return response;

    }

    public Response redeemShoken(String correlationId, String finablrId){
        RestAssured.baseURI="http://104.154.246.25:8080";
        Response response = null;
        RequestSpecification request = given();

        JSONObject childJSON = new JSONObject();
        childJSON.put("FinablrUser001", 0.01);
        childJSON.put("FinablrUser003",0.01);
        childJSON.put("FINABLR",1.0);

        JSONArray array = new JSONArray();
        array.add(childJSON);


        JSONObject requestParams = new JSONObject();
        requestParams.put("correlationId", correlationId);
        requestParams.put("fromFinablrIdsAndQuantities",array);


        response =given()
                .header("Content-Type", "application/json")
                .body(requestParams.toJSONString())
                .post("/api/wallet/redeem-shokens");

        setResp(response);
        return response;

    }
}
