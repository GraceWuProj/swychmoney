package com.swych.swychmoney.test.apis;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.simple.JSONObject;


public class Log extends BaseApi {

    public void getLog(String correlationId, String sender, String recipient, String serviceName,
                       String transactionId, String startTime, String endTime, String limit)
    {
        RestAssured.baseURI="http://35.238.33.22:8080";
        Response response = null;
        try {
            String queryParam = "";
            if (null != correlationId && correlationId.length() != 0) {
                queryParam += "correlationId=" + correlationId + "&";
            }
            if (null != sender && sender.length() != 0) {
                queryParam += "sender=" + sender + "&";
            }
            if (null != recipient && recipient.length() != 0) {
                queryParam += "recipient=" + recipient + "&";
            }
            if (null != serviceName && serviceName.length() != 0) {
                queryParam += "serviceName=" + serviceName + "&";
            }
            if (null != transactionId && transactionId.length() != 0) {
                queryParam += "transactionId=" + transactionId + "&";
            }
            if (null != startTime && startTime.length() != 0) {
                queryParam += "startTime=" + startTime + "&";
            }
            if (null != endTime && endTime.length() != 0) {
                queryParam += "endTime=" + endTime + "&";
            }
            if (null != limit && limit.length() != 0) {
                queryParam += "limit=" + limit + "&";
            }
        //remove the & at the end
             if (queryParam.length() != 0) {
                queryParam = "?" + queryParam.substring(0, queryParam.length()-1);
            }
            response = RestAssured.given()
                    .when()
                    .get("/log" + queryParam);

             setResp(response);

        } catch (Exception e){
            e.printStackTrace();
        }


    }

    public void checkHealth(){
        RestAssured.baseURI="http://35.238.33.22:8080";
        Response response = null;
        response = RestAssured.given()
                .when()
                .get("/actuator/health");

        setResp(response);

    }

}
