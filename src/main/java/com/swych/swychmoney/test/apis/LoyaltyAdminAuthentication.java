package com.swych.swychmoney.test.apis;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import static io.restassured.RestAssured.given;

public class LoyaltyAdminAuthentication extends BaseApi {
    public Response login(String userId, String password){
        RestAssured.baseURI="http://35.226.240.228:8080";
        Response response = null;
        RequestSpecification request = given();

        JSONObject requestParams = new JSONObject();
        requestParams.put("userId", userId);
        requestParams.put("password",password);

        response =given()
                .header("Content-Type", "application/json")
                .body(requestParams.toJSONString())
                .post("/loyalty_admin/v1/authenticate");

        setResp(response);
        return response;

    }
}
