package com.swych.swychmoney.test.apis;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

public class LoyaltyAdminPartners extends BaseApi{

    public Response postPartner(String baseUrl, String newPartnerId,
                                String newName, String newWebsite, String token){
        RestAssured.baseURI = baseURI;
        Response response = null;
        RequestSpecification request = given();
        JSONObject requestParams = new JSONObject();
        requestParams.put("partner_id", newPartnerId);
        requestParams.put("name", newName);
        requestParams.put("website",newWebsite);
        requestParams.put("status", "Active");


        response =given()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + token)
                .body(requestParams.toJSONString())
                .post("/partners");

        setResp(response);
        return response;


    }

    public Response getPartner(String baseURI, String token){
        RestAssured.baseURI = baseURI;
        Response response = null;
        RequestSpecification request = given();

        response =given()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + token)
                .get("/partners");

        setResp(response);
        return response;


    }

    public Response putPartner(String baseURI, String token, String partner_id,
                               String updatedName, String updatedWebsite, Boolean updatedStatus){
        RestAssured.baseURI = baseURI;
        Response response = null;
        RequestSpecification request = given();
        JSONObject requestParams = new JSONObject();
        requestParams.put("partner_id", partner_id);
        requestParams.put("name", updatedName);
        requestParams.put("website",updatedWebsite);
        requestParams.put("status", updatedStatus);

        response =given()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + token)
                .body(requestParams.toJSONString())
                .put("/partners");

        setResp(response);
        return response;

    }
}
