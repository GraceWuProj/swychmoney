package com.swych.swychmoney.test.apis;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import static io.restassured.RestAssured.given;

public class LoyaltyAdminUsers extends BaseApi {

    public Response createUser(String baseURI, String token, String newUserId, String newPassword, String newFirstName,
                               String newLastName, Boolean newReadPermit, Boolean newUpdatePermit, Boolean newAddPermit,
                               Boolean newAdmin, String newStatus  )
    {
        RestAssured.baseURI = baseURI;
        Response response = null;
        RequestSpecification request = given();

        JSONObject requestParams = new JSONObject();
        requestParams.put("user_id", newUserId);
        requestParams.put("password", newPassword);
        requestParams.put("first_name",newFirstName);
        requestParams.put("last_name",newLastName);
        requestParams.put("read",newReadPermit);
        requestParams.put("update",newUpdatePermit);
        requestParams.put("add",newAddPermit);
        requestParams.put("is_admin",newAdmin);
        requestParams.put("status",newStatus);

        response =given()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + token)
                .body(requestParams.toJSONString())
                .post("/users");

        setResp(response);
        return response;

    }

    public Response viewUser(String baseURI, String token){
        RestAssured.baseURI = baseURI;
        Response response = null;
        RequestSpecification request = given();

        response =given()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + token)
                .get("/users");

        setResp(response);
        return response;

    }

    public Response updateUserPassword(String baseURI, String token, String UserId, String updatedPassword )
    {
        RestAssured.baseURI = baseURI;
        Response response = null;
        RequestSpecification request = given();

        JSONObject requestParams = new JSONObject();
        requestParams.put("user_id", UserId);
        requestParams.put("password", updatedPassword);

        response =given()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + token)
                .body(requestParams.toJSONString())
                .put("/users");

        setResp(response);
        return response;
    }

    public Response updateUserFirstname(String baseURI, String token, String UserId, String updatedFirstname )
    {
        RestAssured.baseURI = baseURI;
        Response response = null;
        RequestSpecification request = given();

        JSONObject requestParams = new JSONObject();
        requestParams.put("user_id", UserId);
        requestParams.put("first_name", updatedFirstname);

        response =given()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + token)
                .body(requestParams.toJSONString())
                .put("/users");

        setResp(response);
        return response;
    }

    public Response updateUserLastname(String baseURI, String token, String UserId, String updatedLastname )
    {
        RestAssured.baseURI = baseURI;
        Response response = null;
        RequestSpecification request = given();

        JSONObject requestParams = new JSONObject();
        requestParams.put("user_id", UserId);
        requestParams.put("first_name", updatedLastname);

        response =given()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + token)
                .body(requestParams.toJSONString())
                .put("/users");

        setResp(response);
        return response;
    }

    public Response updateUserReadAddUpdate(String baseURI, String token, String UserId, String updatedReadStatus, String updatedAddStatus, String updatedUpdateStatus )
    {
        RestAssured.baseURI = baseURI;
        Response response = null;
        RequestSpecification request = given();

        JSONObject requestParams = new JSONObject();
        requestParams.put("user_id", UserId);
        requestParams.put("read", updatedReadStatus);
        requestParams.put("add", updatedAddStatus);
        requestParams.put("update", updatedUpdateStatus);

        response =given()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + token)
                .body(requestParams.toJSONString())
                .put("/users");

        setResp(response);
        return response;
    }

    public Response updateUserIsAdmin(String baseURI, String token, String UserId, Boolean updatedIsAdminStatus )
    {
        RestAssured.baseURI = baseURI;
        Response response = null;
        RequestSpecification request = given();

        JSONObject requestParams = new JSONObject();
        requestParams.put("user_id", UserId);
        requestParams.put("is_admin", updatedIsAdminStatus);


        response =given()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + token)
                .body(requestParams.toJSONString())
                .put("/users");

        setResp(response);
        return response;
    }



}
