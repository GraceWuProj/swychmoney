package com.swych.swychmoney.test.apis;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;

public class StellarLoyaltyDistribution extends BaseApi{
    public Response distributeShoken(String externalId){
        RestAssured.baseURI="http://35.223.225.171:9004";
        Response response = null;
        RequestSpecification request = given();

        request.header("Content-Type", "application/json");

        JSONObject childJSON = new JSONObject();
        childJSON.put("account_id", "BGBYWIRIZHGNdKMSLNVYXJIDIHMSMGAA");
        childJSON.put("amount","0.1");
        childJSON.put("account_id", "BGBYWIRIZHGNdKMSLNVYXJIDIHMSMGQR");
        childJSON.put("amount","0.1");



        JSONArray array = new JSONArray();
        array.add(childJSON);

        JSONObject requestParams = new JSONObject();
        requestParams.put("external_id", externalId);
        requestParams.put("token","ShokenB");
        requestParams.put("distributions",array);

        response =given()
                .header("Content-Type", "application/json")
                .body(requestParams.toJSONString())
                .post("/distribution");

        setResp(response);
        return response;


    }

    public Response deleteTran(String externalId){
        RestAssured.baseURI="http://35.223.225.171:9004";
        Response response = null;
        RequestSpecification request = given();

        request.header("Content-Type", "application/json");

        response =given()
                .header("Content-Type", "application/json")
                .delete("/distribution/" + externalId);

        setResp(response);
        return response;

    }

    public Response confirmTran(String externalId){
        RestAssured.baseURI="http://35.223.225.171:9004";
        Response response = null;
        RequestSpecification request = given();

        request.header("Content-Type", "application/json");

        response =given()
                .header("Content-Type", "application/json")
                .put("/distribution/" + externalId);

        setResp(response);
        return response;

    }
    public Response getTran(String externalId){
        RestAssured.baseURI="http://35.223.225.171:9004";
        Response response = null;
        RequestSpecification request = given();

        request.header("Content-Type", "application/json");

        response =given()
                .header("Content-Type", "application/json")
                .get("/distribution/" + externalId);

        setResp(response);
        return response;

    }
    }

