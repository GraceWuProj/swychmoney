package com.swych.swychmoney.test.apis;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import static io.restassured.RestAssured.given;

public class StellarLoyaltyRedeem extends BaseApi{
    public Response redShoken(String externalId, String source_account_id){
        RestAssured.baseURI="http://35.223.225.171:9004";
        Response response = null;
        RequestSpecification request = given();

        JSONObject requestParams = new JSONObject();
        requestParams.put("external_id", externalId);
        requestParams.put("token","ShokenB");
        requestParams.put("source_account_id",source_account_id);
        requestParams.put("amount",0.1);
        response =given()
                .header("Content-Type", "application/json")
                .body(requestParams.toJSONString())
                .post("/redeem");

        setResp(response);
        return response;

    }

    public Response deleteRedemption(String externalId){
        RestAssured.baseURI="http://35.223.225.171:9004";
        Response response = null;
        RequestSpecification request = given();

    /*    JSONObject requestParams = new JSONObject();
        requestParams.put("external_id", externalId);
        requestParams.put("token","ShokenB");
        requestParams.put("source_account_id",source_account_id);*/

        response =given()
                .header("Content-Type", "application/json")
                .delete("/redeem/" + externalId );

        setResp(response);
        return response;

    }

    public Response confirmRedemption(String externalId){
        RestAssured.baseURI="http://35.223.225.171:9004";
        Response response = null;
        RequestSpecification request = given();

        //request.header("Content-Type", "application/json");

    /*    JSONObject requestParams = new JSONObject();
        requestParams.put("external_id", externalId);
        requestParams.put("token","ShokenB");
        requestParams.put("source_account_id",source_account_id);*/

        response =given()
                .header("Content-Type", "application/json")
                .put("/redeem/" + externalId);

        setResp(response);
        return response;

    }



}
