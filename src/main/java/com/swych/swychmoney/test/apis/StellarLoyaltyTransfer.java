package com.swych.swychmoney.test.apis;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import static io.restassured.RestAssured.given;

public class StellarLoyaltyTransfer extends BaseApi{
    public Response transferShoken(String externalId){
        RestAssured.baseURI="http://35.223.225.171:9004";
        Response response = null;
      /*  RequestSpecification request = given();

        request.header("Content-Type", "application/json");*/

        JSONObject childJSON = new JSONObject();
        childJSON.put("account_id", "BGBYWIRIZHGNdKMSLNVYXJIDIHMSMGQR");
        childJSON.put("amount","0.1");




        JSONArray array = new JSONArray();
        array.add(childJSON);

        JSONObject requestParams = new JSONObject();
        requestParams.put("external_id", externalId);
        requestParams.put("token","ShokenB");
        requestParams.put("source_account_id","BGBYWIRIZHGNdKMSLNVYXJIDIHMSMGAA");
        requestParams.put("distributions", array);

        response =given()
                .header("Content-Type", "application/json")
                .body(requestParams.toJSONString())
                .post("/transfer");

        setResp(response);
        return response;
    }

    public Response deleteTransfer(String externalId){
        RestAssured.baseURI="http://35.223.225.171:9004";
        Response response = null;
        RequestSpecification request = given();

        request.header("Content-Type", "application/json");

        response =given()
                .header("Content-Type", "application/json")
                .delete("/transfer/" + externalId);

        setResp(response);
        return response;
    }

    public Response confirmTransfer(String externalId){
        RestAssured.baseURI="http://35.223.225.171:9004";
        Response response = null;
        RequestSpecification request = given();

        request.header("Content-Type", "application/json");

        response =given()
                .header("Content-Type", "application/json")
                .put("/transfer/" + externalId);

        setResp(response);
        return response;
    }
    public Response getTransfer(String externalId){
        RestAssured.baseURI="http://35.223.225.171:9004";
        Response response = null;
        RequestSpecification request = given();

        request.header("Content-Type", "application/json");

        response =given()
                .header("Content-Type", "application/json")
                .get("/transfer/" + externalId);

        setResp(response);
        return response;
    }

}
