package com.swych.swychmoney.test.cases;



import com.swych.swychmoney.test.apis.CordaMint;
import com.swych.swychmoney.test.apis.StellarLoyaltyDistribution;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Parameters;

public class CheckMintBalance {

    @Test (priority = 1)
    public void viewCirculatingSupply (){
        CordaMint  cordaMint = new CordaMint();
        Response response = cordaMint.getCirculatingSupply();
        System.out.println("viewCirculatingSupply");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(cordaMint.getStatusCode(), 200);
    }


    @Test (priority = 2)
    public void viewMintBalance (){
        CordaMint  cordaMint = new CordaMint();
        Response response = cordaMint.getMintBalance();
        System.out.println("viewMintBalance");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(cordaMint.getStatusCode(), 200);
    }

}
