package com.swych.swychmoney.test.cases;

import com.swych.swychmoney.test.apis.CordaLoyaltyTransaction;
import com.swych.swychmoney.test.apis.CordaMint;
import com.swych.swychmoney.test.apis.CordaWallet;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.math.BigDecimal;

public class CreateAccCreateTransTransferShoken {

    private String finablrId;
    private String correlationId;
    private BigDecimal nodeBalanceBefore;
    private BigDecimal balanceBefore;
    private int num;
    private String extTxId;

   @Test (priority = 1)
    public void createNewAcc(){
        num = Utils.getRandom();
        finablrId = "TestAcc" + num;
        CordaWallet cordaWallet = new CordaWallet();
        Response response = cordaWallet.createAccount(finablrId);
        System.out.println("createNewAcc");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(cordaWallet.getStatusCode(), 200);
    }

    @Test (priority = 2)
    public void getNewAcc(){
        CordaWallet cordaWallet = new CordaWallet();
        Response response = cordaWallet.getAccount(finablrId);
        System.out.println("getNewAcc");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(cordaWallet.getStatusCode(), 200);
    }

    @Test(priority = 3)
    public void checkNodeBalance() {
        CordaMint cordaMint = new CordaMint();
        cordaMint.getMintBalance();
        nodeBalanceBefore = cordaMint.getNodeBalance();
        System.out.println("nodeBalanceBefore: " + nodeBalanceBefore.doubleValue());
    }

    @Parameters({"extTxProduct"})
    @Test(priority = 4)
    public void createLoyaltyTrans(String extTxProduct) {
        num = Utils.getRandom();
        correlationId = Utils.getUuid();
        extTxId = "tx" + num;
        String extTxTimestamp = Utils.getTimeStamp();
        CordaLoyaltyTransaction cordaLoyaltyTransaction = new CordaLoyaltyTransaction();
        Response response = cordaLoyaltyTransaction.createTrans(correlationId, extTxProduct, extTxId, extTxTimestamp);
        System.out.println("createLoyaltyTrans: " + correlationId);
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(cordaLoyaltyTransaction.getStatusCode(), 200);
    }

    @Test(priority = 5)
    public void checkNodeBalanceAfter() {
        CordaMint cordaMint = new CordaMint();
        cordaMint.getMintBalance();
        BigDecimal nodeBalanceAfter = cordaMint.getNodeBalance();
        System.out.println("nodeBalanceAfter: " + nodeBalanceAfter);
        BigDecimal balanceDifference = this.nodeBalanceBefore.subtract(nodeBalanceAfter).multiply(new BigDecimal(100));
        assert( balanceDifference.intValue() == 102 );
    }
    @Parameters({"extTxProduct"})
    @Test(priority = 6)
    public void getTransaction(String extTxProduct) {
         CordaLoyaltyTransaction cordaLoyaltyTransaction = new CordaLoyaltyTransaction();
        Response response = cordaLoyaltyTransaction.viewTransaction(extTxProduct, extTxId);
        String extTxProductReturned = cordaLoyaltyTransaction.getextTxProduct();
        String extTxIdReturned = cordaLoyaltyTransaction.getextTxId();
        System.out.println("getTransaction: " + extTxProductReturned + " " + extTxIdReturned);
        Assert.assertEquals(extTxProductReturned, extTxProduct);
        Assert.assertEquals(extTxIdReturned, extTxId);
    }

    @Test (priority = 7)
    public void checkUserBalanceBefore() {
        CordaWallet cordaWallet = new CordaWallet();
        cordaWallet.getAccountBalance("AutoTestUser001");
        balanceBefore = cordaWallet.getAccBalance();
        System.out.println("balanceBefore: " + balanceBefore);
    }

    @Test(priority = 8)
    public void transShokens() {
        correlationId = Utils.getUuid();
        CordaWallet cordaWallet = new CordaWallet();
        Response response = cordaWallet.transferShoken(correlationId);
        System.out.println("transferShoken: " + correlationId);
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(cordaWallet.getStatusCode(), 200);

    }

    @Test(priority = 9)
    public void checkUserBalance() {
        CordaWallet cordaWallet = new CordaWallet();
        cordaWallet.getAccountBalance("AutoTestUser001");
        BigDecimal balanceAfter = cordaWallet.getAccBalance();
        System.out.println("balanceAfter: " + balanceAfter);
        BigDecimal balanceDifference = this.balanceBefore.subtract(balanceAfter).multiply(new BigDecimal(1000));
        //Assert.assertEquals(Math.rint((double)(balanceDifference*1000)),2);
        assert( balanceDifference.intValue() == 2 );
    }

}
