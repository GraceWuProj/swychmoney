package com.swych.swychmoney.test.cases;

import com.swych.swychmoney.test.apis.LoyaltyAdminPartners;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class CreateUpdateRetrievePartner {
    String newPartner_id = null;
    String newName = null;
    String newWebsite = null;
    @Parameters({"baseUrl", "token"})
    @Test
    public void createPartner(String baseUrl, String token) {
        newPartner_id = "";
        newName = "";
        newWebsite = "";
        LoyaltyAdminPartners loyaltyAdminPartners = new LoyaltyAdminPartners();
        Response response = loyaltyAdminPartners.postPartner(baseUrl, newPartner_id, newName, newWebsite, token);
        System.out.println("createPartner");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(loyaltyAdminPartners.getStatusCode(), 200);
        Assert.assertEquals(loyaltyAdminPartners.getPartner_id(), "newPartner_id");
    }

/*   @Test
    public void viewPartner(String baseUrl, String token) {
        LoyaltyAdminPartners loyaltyAdminPartners = new LoyaltyAdminPartners();
        Response response = loyaltyAdminPartners.getPartner(baseUrl, token);
        System.out.println("viewPartner");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(loyaltyAdminPartners.getStatusCode(), 200);
        response.getBody().

    }*/

}
