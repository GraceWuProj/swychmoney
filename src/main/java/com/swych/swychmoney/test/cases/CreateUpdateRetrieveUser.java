package com.swych.swychmoney.test.cases;

import com.swych.swychmoney.test.apis.LoyaltyAdminAuthentication;
import com.swych.swychmoney.test.apis.LoyaltyAdminUsers;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class CreateUpdateRetrieveUser {
    String token = null;
    String UserId = null;
    String NewPassword = null;
    String NewFirstName = null;
    String NewLastname = null;
    String NewReadPermit = null;
    String NewUpdatePermit = null;
    String NewAddPermit = null;
    String NewAdmin = null;
    String NewStatus = null;

    @Test (priority = 1)
    @Parameters ({"userId", "password"})
    public void loginAccount(String userId, String password){
        LoyaltyAdminAuthentication auth = new LoyaltyAdminAuthentication();
        Response response = auth.login(userId, password);
        System.out.println("loginAccount");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(auth.getStatusCode(), 200);
       // Assert.assertEquals(auth.getUserId(), userId);
        Assert.assertNotNull(auth.getAccessToken());
        token = auth.getAccessToken();
    }

    @Test (priority = 2)
    @Parameters ({"baseURI", "newUserId", "newPassword", "newFirstName",
            "newLastName", "newReadPermit", "newUpdatePermit", "newAddPermit",
            "newAdmin", "newStatus"})
    public void postUser(String baseURI, String newUserId, String newPassword, String newFirstName,
                         String newLastName, Boolean newReadPermit, Boolean newUpdatePermit, Boolean newAddPermit,
                         Boolean newAdmin, String newStatus){
        LoyaltyAdminUsers user = new LoyaltyAdminUsers();
        Response response = user.createUser(baseURI, token, newUserId, newPassword, newFirstName,
                newLastName, newReadPermit, newUpdatePermit, newAddPermit,
                newAdmin, newStatus);
        System.out.println("postUser");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(user.getStatusCode(), 200);
        Assert.assertEquals(user.getUserId(), newUserId);
        Assert.assertEquals(user.getFirstname(), newFirstName);
        Assert.assertEquals(user.getLastname(), newLastName);
        Assert.assertEquals(user.getUpdatestatus(), newUpdatePermit.toString());
        Assert.assertEquals(user.getReadstatus(), newReadPermit.toString());
        Assert.assertEquals(user.getAddstatus(), newAddPermit.toString());
        Assert.assertEquals(user.getIsadminstatus(), newAdmin.toString());
        Assert.assertEquals(user.getStatus(), newStatus.toString());
        UserId = newUserId;
        NewFirstName = newFirstName;
        NewLastname = newLastName;
        NewReadPermit = newReadPermit.toString();
        NewUpdatePermit = newReadPermit.toString();
        NewAddPermit = newAddPermit.toString();
        NewAdmin = newAdmin.toString();
        NewStatus = newStatus;

    }

    @Test (priority = 3)
    @Parameters ({"baseURI"})
    public void getUser1(String baseURI){
        LoyaltyAdminUsers user = new LoyaltyAdminUsers();
        Response response = user.viewUser(baseURI, token);
        System.out.println("gerUser1");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(user.getStatusCode(), 200);

    }

    @Test (priority = 4)
    @Parameters ({"baseURI", "updatedPassword"})
    public void putUserPassword(String baseURI, String newUserId, String updatedPassword){
        LoyaltyAdminUsers user = new LoyaltyAdminUsers();
        Response response = user.updateUserPassword(baseURI, token, newUserId, updatedPassword);
        System.out.println("putUserPassword");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(user.getStatusCode(), 200);
        Assert.assertEquals(user.getUserId(), UserId);
        Assert.assertNotNull(user.getPassword());
        Assert.assertEquals(user.getFirstname(), NewFirstName);
        Assert.assertEquals(user.getLastname(), NewLastname);
        Assert.assertEquals(user.getAddstatus(), NewAddPermit);
        Assert.assertEquals(user.getReadstatus(), NewReadPermit);
        Assert.assertEquals(user.getUpdatestatus(), NewUpdatePermit);
        Assert.assertEquals(user.getStatus(), NewStatus);
        Assert.assertEquals(user.getIsadminstatus(), NewAdmin);
    }

    @Test (priority = 5)
    @Parameters ({"baseURI", "updatedFirstname"})
    public void putUserFirstname(String baseURI, String newUserId, String updatedFirstname){
        LoyaltyAdminUsers user = new LoyaltyAdminUsers();
        Response response = user.updateUserFirstname(baseURI, token, newUserId, updatedFirstname);
        System.out.println("putUserFirstname");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(user.getStatusCode(), 200);
        Assert.assertEquals(user.getUserId(), newUserId);
        Assert.assertEquals(user.getFirstname(), updatedFirstname);
        Assert.assertEquals(user.getLastname(), NewLastname);
        Assert.assertEquals(user.getAddstatus(), NewAddPermit);
        Assert.assertEquals(user.getReadstatus(), NewReadPermit);
        Assert.assertEquals(user.getUpdatestatus(), NewUpdatePermit);
        Assert.assertEquals(user.getStatus(), NewStatus);
        Assert.assertEquals(user.getIsadminstatus(), NewAdmin);

    }

    @Test (priority = 6)
    @Parameters ({"baseURI", "updatedLastname"})
    public void putUserLastname(String baseURI, String newUserId, String updatedLastname){
        LoyaltyAdminUsers user = new LoyaltyAdminUsers();
        Response response = user.updateUserFirstname(baseURI, token, newUserId, updatedLastname);
        System.out.println("putUserLastname");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(user.getStatusCode(), 200);
        Assert.assertEquals(user.getUserId(), newUserId);
        Assert.assertEquals(user.getLastname(), updatedLastname);
        Assert.assertEquals(user.getAddstatus(), NewAddPermit);
        Assert.assertEquals(user.getReadstatus(), NewReadPermit);
        Assert.assertEquals(user.getUpdatestatus(), NewUpdatePermit);
        Assert.assertEquals(user.getStatus(), NewStatus);
        Assert.assertEquals(user.getIsadminstatus(), NewAdmin);

    }

    @Test (priority = 7)
    @Parameters ({"baseURI", "updatedAddstatus", "updatedReadstatus", "updatedUpdateStatus"})
    public void putUserAddReadUpdatestatus(String baseURI, String newUserId, String updatedAddstatus, String updatedReadstatus, String updatedUpdatestatus){
        LoyaltyAdminUsers user = new LoyaltyAdminUsers();
        Response response = user.updateUserReadAddUpdate(baseURI, token, newUserId, updatedAddstatus, updatedReadstatus, updatedUpdatestatus );
        System.out.println("putUserAddReadUpdatestatus");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(user.getStatusCode(), 200);
        Assert.assertEquals(user.getUserId(), newUserId);
        Assert.assertEquals(user.getAddstatus(), updatedAddstatus);
        Assert.assertEquals(user.getReadstatus(), updatedReadstatus);
        Assert.assertEquals(user.getUpdatestatus(), updatedUpdatestatus);
        Assert.assertEquals(user.getStatus(), NewStatus);
        Assert.assertEquals(user.getIsadminstatus(), NewAdmin);

    }



}
