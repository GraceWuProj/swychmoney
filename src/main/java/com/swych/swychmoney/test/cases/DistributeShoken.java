package com.swych.swychmoney.test.cases;

import com.swych.swychmoney.test.apis.StellarLoyaltyDistribution;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DistributeShoken {
    private String uuid;
    @Test (priority = 1)
    public void distriShoken (){
        uuid = Utils.getUuidWithoutDash();
        StellarLoyaltyDistribution dis = new StellarLoyaltyDistribution();
        Response response = dis.distributeShoken(uuid);
        System.out.println("distriShoken");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(dis.getStatus(), "Queued");
    }
    @Test (priority = 2)
    public void rollbackTran () {
        StellarLoyaltyDistribution dis = new StellarLoyaltyDistribution();
        Response response = dis.deleteTran(uuid);
        System.out.println("rollbackTran");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(dis.getStatus(), "RolledBack");
    }
    @Test (priority = 3)
    public void getTran1 () {
        StellarLoyaltyDistribution dis = new StellarLoyaltyDistribution();
        Response response = dis.getTran(uuid);
        System.out.println("getTran1");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(dis.getStatus(), "RolledBack");
    }
    @Test (priority = 4)
    public void distriShoken2 (){
        uuid = Utils.getUuidWithoutDash();
        StellarLoyaltyDistribution dis = new StellarLoyaltyDistribution();
        Response response = dis.distributeShoken(uuid);
        System.out.println("distriShoken2");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(dis.getStatus(), "Queued");
    }
    @Test (priority = 5)
    public void commitTran () {
        StellarLoyaltyDistribution dis = new StellarLoyaltyDistribution();
        Response response = dis.confirmTran(uuid);
        System.out.println("commitTran");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(dis.getStatus(), "Committed");
    }
    @Test (priority = 6)
    public void getTran2 () {
        StellarLoyaltyDistribution dis = new StellarLoyaltyDistribution();
        Response response = dis.getTran(uuid);
        System.out.println("getTran2");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(dis.getStatus(), "Committed");
    }


}
