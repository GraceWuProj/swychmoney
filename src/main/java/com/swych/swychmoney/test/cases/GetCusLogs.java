package com.swych.swychmoney.test.cases;

import com.swych.swychmoney.test.apis.Log;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GetCusLogs {

    @Test
    public void getLog1() {
        String correlationId = null, sender = null, recipient = null;
        String serviceName = null, transactionId = null, startTime = null;
        String endTime = null, limit =null;

        Log l = new Log();
        l.getLog(correlationId, sender, recipient, serviceName, transactionId, startTime, endTime, limit);
        if (l.getStatusCode() != 200)
            System.out.println("getLog1 gets "+ l.getStatusCode());
        Assert.assertEquals(l.getStatusCode(), 200);
    }

    @Test
    public void getLog2() {
        String correlationId = "897b7f44-1f31-4c95-80cb-bbb43e4dcf05", sender = null, recipient = null;
        String serviceName = null, transactionId = null, startTime = null;
        String endTime = null, limit =null;

        Log l = new Log();
        l.getLog(correlationId, sender, recipient, serviceName, transactionId, startTime, endTime, limit);
        if (l.getStatusCode() != 200)
            System.out.println("getLog2 gets "+ l.getStatusCode());
        Assert.assertEquals(l.getStatusCode(), 200);
    }

    @Test
    public void getLog3() {
        String correlationId = null, sender = "rajanrabbit", recipient = null;
        String serviceName = null, transactionId = null, startTime = null;
        String endTime = null, limit =null;
        Log l = new Log();
        l.getLog(correlationId, sender, recipient, serviceName, transactionId, startTime, endTime, limit);
        if (l.getStatusCode() != 200)
            System.out.println("getLog3 gets "+ l.getStatusCode());
        Assert.assertEquals(l.getStatusCode(), 200);
    }

    @Test
    public void getLog4() {
        String correlationId = null, sender = null, recipient = "recipient4";
        String serviceName = null, transactionId = null, startTime = null;
        String endTime = null, limit =null;
        Log l = new Log();
        l.getLog(correlationId, sender, recipient, serviceName, transactionId, startTime, endTime, limit);
        if (l.getStatusCode() != 200)
            System.out.println("getLog4 gets "+ l.getStatusCode());
        Assert.assertEquals(l.getStatusCode(), 200);
    }

    @Test
    public void getLog5() {
        String correlationId = null, sender = null, recipient = null;
        String serviceName = "rabbitmq", transactionId = null, startTime = null;
        String endTime = null, limit =null;
         Log l = new Log();
         l.getLog(correlationId, sender, recipient, serviceName, transactionId, startTime, endTime, limit);
        if (l.getStatusCode() != 200)
            System.out.println("getLog5 gets "+ l.getStatusCode());
         Assert.assertEquals(l.getStatusCode(), 200);
     }
    @Test
    public void getLog6() {
        String correlationId = null, sender = null, recipient = null;
        String serviceName = null, transactionId = "237b7f44-1f31-4c95-80cb-bbb43e4dcf05", startTime = null;
        String endTime = null, limit =null;
        Log l = new Log();
        l.getLog(correlationId, sender, recipient, serviceName, transactionId, startTime, endTime, limit);
        if (l.getStatusCode() != 200)
            System.out.println("getLog6 gets "+ l.getStatusCode());
        Assert.assertEquals(l.getStatusCode(), 200);
    }
    @Test
    public void getLog7() {
        String correlationId = null, sender = null, recipient = null;
        String serviceName = null, transactionId = null, startTime = null;
        String endTime = null, limit = "2";
        Log l = new Log();
        l.getLog(correlationId, sender, recipient, serviceName, transactionId, startTime, endTime, limit);
        if (l.getStatusCode() != 200)
            System.out.println("getLog7 gets "+ l.getStatusCode());
        Assert.assertEquals(l.getStatusCode(), 200);
        Assert.assertEquals(l.getNumberRec(), limit);
    }
     @Test
    public void getLog8() {
         String correlationId = null, sender = null, recipient = null;
         String serviceName = null, transactionId = null, startTime = "2019-10-05T13:00:00+00:00";
         String endTime = null, limit =null;
         Log l = new Log();
         l.getLog(correlationId, sender, recipient, serviceName, transactionId, startTime, endTime, limit);
         if (l.getStatusCode() != 200)
             System.out.println("getLog8 gets "+ l.getStatusCode());
         Assert.assertEquals(l.getStatusCode(), 200);
     }
     @Test
    public void getLog9() {
         String correlationId = null, sender = null, recipient = null;
         String serviceName = null, transactionId = null, startTime = null;
         String endTime = "2019-10-15T13:00:00+00:00", limit =null;
         Log l = new Log();
         l.getLog(correlationId, sender, recipient, serviceName, transactionId, startTime, endTime, limit);
         if (l.getStatusCode() != 200)
             System.out.println("getLog9 gets "+ l.getStatusCode());
         Assert.assertEquals(l.getStatusCode(), 200);
     }
    @Test
     public void checkHealth() {
        Log l = new Log();
        l.checkHealth();
        if (l.getStatusCode() != 200)
            System.out.println("checkHealth gets "+ l.getStatusCode());
        Assert.assertEquals(l.getStatusCode(), 200);
        Assert.assertEquals(l.getStatus(), "UP");
    }

    }

