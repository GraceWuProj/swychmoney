package com.swych.swychmoney.test.cases;



import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.swych.swychmoney.test.apis.LoyaltyAdminAuthentication;
import io.restassured.response.Response;

public class LoginLoyaltyAdminAcc {

    @Test
    @Parameters ({"userId", "password"})
    public void loginAccount(String userId, String password){
        LoyaltyAdminAuthentication auth = new LoyaltyAdminAuthentication();
        Response response = auth.login(userId, password);
        System.out.println("loginAccount");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(auth.getStatusCode(), 200);
        Assert.assertEquals(auth.getUserId(), userId);
        Assert.assertNotNull(auth.getAccessToken());

    }
}
