package com.swych.swychmoney.test.cases;

import com.swych.swychmoney.test.apis.StellarLoyaltyDistribution;
import com.swych.swychmoney.test.apis.StellarLoyaltyRedeem;
import com.swych.swychmoney.test.stellar.PostgreSqlServer;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.sql.SQLException;

public class RedeemShokenConfirm {
    private String uuid;
    private String source_account_id;
    private long ledger;

    @Test (priority = 1)
    public void redeemShoken1 () throws SQLException {

        uuid = Utils.getUuidWithoutDash();
        source_account_id = "7487412806fc41d6b72893efe3a455cc";
        StellarLoyaltyRedeem red = new StellarLoyaltyRedeem();
        Response response = red.redShoken(uuid, source_account_id);
        System.out.println("redeemShoken");

        PostgreSqlServer p = new PostgreSqlServer();
        ledger = p.getLedgerId(uuid);
        System.out.println("ledgerId: " + ledger);

        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(red.getStatus(), "Queued");
    }

    @Test (priority = 2)
    public void delRedemption (){

        StellarLoyaltyRedeem del = new StellarLoyaltyRedeem();
        Response response = del.deleteRedemption(uuid);
        System.out.println("delRedemption");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(del.getStatus(), "RolledBack");
    }

 /*   @Test (priority = 3)
    public void redeemShoken2 (){

        uuid = Utils.getUuidWithoutDash();
        source_account_id = "7487412806fc41d6b72893efe3a455cc";
        StellarLoyaltyRedeem red = new StellarLoyaltyRedeem();
        Response response = red.redShoken(uuid, source_account_id);
        System.out.println("redeemShoken2");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(red.getStatus(), "Queued");
    }

    @Test (priority = 4)
    public void conRedemption (){

        StellarLoyaltyRedeem con = new StellarLoyaltyRedeem();
        Response response = con.confirmRedemption(uuid);
        System.out.println("conRedemption");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(con.getStatus(), "Committed");
    }*/


}
