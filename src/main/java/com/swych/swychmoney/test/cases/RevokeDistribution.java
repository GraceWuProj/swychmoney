package com.swych.swychmoney.test.cases;

import com.beust.jcommander.Parameter;
import com.swych.swychmoney.test.apis.StellarLoyaltyDistribution;
import com.swych.swychmoney.test.apis.StellarLoyaltyRevoke;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RevokeDistribution {
    private String uuid;
    //distribute Shoken
    @Test (priority = 1)
    public void distriShoken (){
        uuid = Utils.getUuidWithoutDash();
        StellarLoyaltyDistribution dis = new StellarLoyaltyDistribution();
        Response response = dis.distributeShoken(uuid);
        System.out.println("distriShoken");
        Utils.getJsonPrettyPrint(response.getBody().asString());
       // Assert.assertEquals(dis.getStatus(), "Queued");
    }
    //confirm distribution with the same external_id
    @Test (priority = 2)
    public void confirmDistri(){
        StellarLoyaltyDistribution dis = new StellarLoyaltyDistribution();
        Response response = dis.confirmTran(uuid);
        System.out.println("confirmDistri");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(dis.getStatus(), "Committed");
    }
    //get distribution with the same external_id
    @Test (priority = 3)
    public void getDistri(){
        StellarLoyaltyDistribution dis = new StellarLoyaltyDistribution();
        Response response = dis.getTran(uuid);
        System.out.println("getDistri");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(dis.getStatus(), "Committed");
    }
    //revoke distribution
    @Test (priority = 4)
    public void revDistri1(){
        uuid = Utils.getUuidWithoutDash();
        StellarLoyaltyRevoke rev = new StellarLoyaltyRevoke();
        Response response = rev.revDistribution(uuid);
        System.out.println("revDistri1");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(rev.getStatus(), "Queued");
    }
    //revert the revocation with the same external_id
    @Test (priority = 5)
    public void deleteRev(){
        StellarLoyaltyRevoke rev = new StellarLoyaltyRevoke();
        Response response = rev.deleteRev(uuid);
        System.out.println("deleteRev");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(rev.getStatus(), "RolledBack");
    }
    //get the deleted revocation
    @Test (priority = 6)
    public void getRevRolledback(){
        StellarLoyaltyRevoke rev = new StellarLoyaltyRevoke();
        Response response = rev.getRev(uuid);
        System.out.println("getRevRolledback");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(rev.getStatus(), "RolledBack");
    }
    //revoke distribution with a new external_id
    @Test (priority = 7)
    public void revDistri2(){
        uuid = Utils.getUuidWithoutDash();
        StellarLoyaltyRevoke rev = new StellarLoyaltyRevoke();
        Response response = rev.revDistribution(uuid);
        System.out.println("revDistri2");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(rev.getStatus(), "Queued");

    }
    //confirm the revocation with the same external_id
    @Test (priority = 8)
    public void confirmRev(){
        StellarLoyaltyRevoke rev = new StellarLoyaltyRevoke();
        Response response = rev.confirmRev(uuid);
        System.out.println("confirmRev");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(rev.getStatus(), "Committed");
    }
    //get the confirmed revocation with the same external_id
    @Test (priority = 9)
    public void getRevCommitted(){
        StellarLoyaltyRevoke rev = new StellarLoyaltyRevoke();
        Response response = rev.getRev(uuid);
        System.out.println("getRevCommitted");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(rev.getStatus(), "Committed");
    }

}
