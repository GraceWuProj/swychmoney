package com.swych.swychmoney.test.cases;

import com.swych.swychmoney.test.apis.StellarLoyaltyTransfer;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TransferShoken {
    // generate uuid as external_id and transfer shoken
    private String uuid;
    @Test (priority = 1)
    public void transShoken1(){
        uuid = Utils.getUuidWithoutDash();
        StellarLoyaltyTransfer trans = new StellarLoyaltyTransfer();
        Response response = trans.transferShoken(uuid);
        System.out.println("transShoken1");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(trans.getStatus(), "Queued");
    }
    // get transfer with the same external_id
    @Test (priority = 2)
    public void getTransQueued(){
        StellarLoyaltyTransfer trans = new StellarLoyaltyTransfer();
        Response response = trans.getTransfer(uuid);
        System.out.println("getTransQueued");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(trans.getStatus(), "Queued");
    }
    //rollback transfer with the same external_id
    @Test (priority = 3)
    public void rollbackTransfer(){
        StellarLoyaltyTransfer trans = new StellarLoyaltyTransfer();
        Response response = trans.deleteTransfer(uuid);
        System.out.println("rollbackTransfer");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(trans.getStatus(), "RolledBack");
    }
   //retrieve transfer with the same external_id
    @Test (priority = 4)
    public void getTranDeleted(){
        StellarLoyaltyTransfer trans = new StellarLoyaltyTransfer();
        Response response = trans.getTransfer(uuid);
        System.out.println("getTranDeleted");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(trans.getStatus(), "RolledBack");
    }
    // generate a new external_id then transfer shoken
    @Test (priority = 5)
    public void transShoken2(){
        uuid = Utils.getUuidWithoutDash();
        StellarLoyaltyTransfer trans = new StellarLoyaltyTransfer();
        Response response = trans.transferShoken(uuid);
        System.out.println("transShoken2");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(trans.getStatus(), "Queued");
    }
    //confirm transfer with the same external_id
    @Test (priority = 6)
    public void commitTran(){
        StellarLoyaltyTransfer trans = new StellarLoyaltyTransfer();
        Response response = trans.confirmTransfer(uuid);
        System.out.println("commitTran");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(trans.getStatus(), "Committed");
    }
    //get the confirmed transfer
    @Test (priority = 7)
    public void getTranQueued2(){
        StellarLoyaltyTransfer trans = new StellarLoyaltyTransfer();
        Response response = trans.getTransfer(uuid);
        System.out.println("getTranQueued2");
        Utils.getJsonPrettyPrint(response.getBody().asString());
        Assert.assertEquals(trans.getStatus(), "Committed");

    }
}
