package com.swych.swychmoney.test.cases;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.security.Timestamp;
import java.util.Date;
import java.util.Random;
import java.util.UUID;


public class Utils {
    // pretty print out the JSON
    public static String getJsonPrettyPrint(String jsonString) {
        JsonElement jsonElement = new JsonParser().parse(jsonString);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(jsonElement);
    }

    public static String getUuidWithoutDash() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    public static int getRandom() {
        Random random = new Random();
        int ranInt = random.nextInt(1000);
        return ranInt;
    }

    public static String getTimeStamp(){
        java.util.Date t = new java.util.Date(System.currentTimeMillis() - 24*3600*1000);
        String time = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").format(t);
        return time;
    }

    public static String getUuid(){return UUID.randomUUID().toString();}

}

