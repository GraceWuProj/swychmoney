package com.swych.swychmoney.test.stellar;

import java.sql.*;

public class PostgreSqlServer {

    public long getLedgerId(String externalId) {
        long transactionLedgerId = 0;

        String url = "jdbc:postgresql://localhost:8101/stellar_queue";
        String user = "qa_user";
        String password = "swychpgsql_qa";

        try {
            Connection con = DriverManager.getConnection(url, user, password);
            String selectSql = "select transaction_ledger_id from pendingtransaction where transaction_external_id='"
                        + externalId + "' order by transaction_created desc limit 1";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(selectSql);
            if (rs.next()) {
                transactionLedgerId = rs.getLong(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return transactionLedgerId;
    }

    public static void main(String[] args) {
        PostgreSqlServer p = new PostgreSqlServer();
        long ledger = p.getLedgerId("2e65ec4cc5f64f4fb43f2c169f98e6e8");
        System.out.print(ledger);
    }
}
